# Magic

**(Don't tell your tricks...)**

This book is for documenting all unfinished or fringe features which are important enough to document (all are) but are not ready for customer use or are too weird to explain. So this should be a strictly internal document.




This book is written with mdbook. You can find information about the tool [here: https://rust-lang.github.io/mdBook/index.html](https://rust-lang.github.io/mdBook/index.html)
