# Asset Magic

## Syncing Assets to the Box

```json
{
    "sync_assets": "asset_dir_name"    
}
```
asset_dir_name must exist on PanooCentral (https://central.panoo.tv/assets/asset_dir_name/). The Box will sync all Files int the directory to the local directory `/storage/assets`. "sync_assets" can also be used in the Family config in which case all files from both the family and the box sync dir will be synced.

## Channel URLs for local assets

asset:filename.txt

### Channel URLs for system assets
System assets in /usr/share/panoo/assets can be used with panoo: URLs


## Root Certificates

### customer.crt

### certs

## SSH Authorized Keys
If a file named `authorized_keys`gets synced to the box, its contents wil get appended to the boxes authorized_keys file so that SSH access will be enabled for users identified by the included public keys.











